---
hostname: 'n9kv-21-leaf'
os: nxos
chassis: n9kv

loopback: '10.0.0.21/32'
gateway: '192.168.1.1'
mgmt_ip: '192.168.1.71/24'
vdc:
    name: "{{ hostname }}"
    id: 1


interfaces:
    switch:
        - 
            name: Ethernet1/2
            mode: trunk
            port-channel: 99
        - 
            name: Ethernet1/4
            mode: access
            vlan: 
                - 100
        - 
            name: Ethernet1/5
            mode: access
            vlan: 
                - 101
        - 
            name: Ethernet1/6
            mode: access
            vlan: 
                - 104
        - 
            name: port-channel4
            mode: access
            vlan: 
                - 100
        - 
            name: port-channel99
            mode: trunk

    l3:
        -
            name: mgmt0
            ipv4: "{{ mgmt_ip }}"
        - 
            name: loopback0
            ipv4: "{{ loopback }}"
            primary: true
        - 
            name: Ethernet1/1
            ipv4: '10.11.21.1/31'
        - 
            name: vlan100
            ipv4: '10.21.100.21/24'
        - 
            name: vlan101
            ipv4: '10.21.121.1/24'
        - 
            name: vlan104
            ipv4: '10.21.221.1/24'


routes:
    - 
        route: 0.0.0.0/0
        next-hop: 192.168.1.1
        vrf: management

ospf:
    areas:
        0.0.0.0:
            interfaces:
                - 
                    name: all
                    ospftype: p2p
                - 
                    name: lo0.0
                    ospftype: passive
                - 
                    name: em0.0
                    ospftype: disable
                - 
                    name: em1.0
                    ospftype: disable

ldp:
    interfaces:
        - 
            name: all
            mode: enable
        - 
            name: em0.0
            mode: disable
        - 
            name: em1.0
            mode: disable
            
mpls:
    interfaces:
        - 
            name: all
            mode: enable
        - 
            name: em0.0
            mode: disable
        - 
            name: em1.0
            mode: disable

    traffic-engineering: bgp-igp-both-ribs
    icmp-tunneling: true



pim:
    interfaces:
        - 
            name: all
            mode: sparse
        - 
            name: em0.0
            mode: disable
        - 
            name: em1.0
            mode: disable


bgp:
    asn: 64000
    groups:
        ibgp:
            bgp_type: internal
            peer-asn: 64000
            multihop: true
            local-address: 10.0.0.21
            families:
                - evpn
            is_rr: false
            neighbors:
                - 10.0.0.11



services:
    vr:
        - 
            name: management
            interfaces:
                - mgmt0
        
    evpn5:
        -
            name: vxlan-110
            interfaces: 
                - vlan101
                - vlan104
            route-map: RM-VXLAN-110
            rd: 10.0.0.21:110
            rt: 64000:110
            dummy-vlan: 1101
            vxlan:
                vni: 20110

    evpn2:
        - 
            name: vxlan-100
            interfaces:
                - ge-0/0/6.0
            rd: 10.0.0.21:100
            rt: 64200:100
            l3vni: 20100
            vni-list: 
                - 10100
                - 10200
            bridge-domain:
                -
                    vlan: 100
                    l3-interface: irb.100
                    vni: 10100
                    replication: ingress-node-replication
                -
                    vlan: 200
                    l3-interface: irb.200
                    vni: 10200
                    replication: ingress-node-replication

    

vlans:
    - 
        name: default
        id: 1
    - 
        name: vlan-101
        id: 101
    - 
        name: vlan-100
        id: 100
        vni: 10100
    - 
        name: vlan-200
        id: 200
        vni: 10200
