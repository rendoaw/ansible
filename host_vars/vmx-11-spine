---
hostname: 'vmx-11-spine'
os: junos
chassis: vmx

loopback: '10.0.0.11/32'
gateway: '192.168.1.1'
mgmt_ip: '192.168.1.80/24'

interfaces:
    bridge:
        - 
            name: ge-0/0/6
            unit: 0
            mode: access
            vlan: 
                - 100

    l3:
        - 
            name: lo0
            unit: 0
            ipv4: "{{ loopback }}"
            mpls: true
        - 
            name: lo0
            unit: 901
            ipv4: '9.0.0.11/32'
        - 
            name: ge-0/0/0
            unit: 0
            ipv4: '10.11.21.0/31'
            mpls: true
        - 
            name: ge-0/0/1
            unit: 0
            ipv4: '10.11.22.0/31'
            mpls: true
        - 
            name: ge-0/0/2
            unit: 0
            ipv4: '10.11.23.0/31'
            mpls: true
        - 
            name: ge-0/0/3
            unit: 0
            ipv4: '10.11.24.0/31'
            mpls: true
        - 
            name: ge-0/0/4
            unit: 0
            ipv4: '10.11.25.0/31'
            mpls: true
        - 
            name: ge-0/0/5
            unit: 0
            ipv4: '10.11.26.0/31'
            mpls: true
        - 
            name: ge-0/0/7
            unit: 0
            ipv4: '10.11.28.0/31'
            mpls: true
        - 
            name: ge-0/0/8
            unit: 0
            ipv4: '10.11.27.0/31'
            mpls: true
        - 
            name: ge-0/0/10
            unit: 0
            ipv4: '10.11.29.0/31'
            mpls: true
        - 
            name: ge-0/0/11
            unit: 0
            ipv4: '10.11.93.1/30'
            mpls: true
        - 
            name: ge-0/0/12
            unit: 0
            ipv4: '10.11.94.1/30'
            mpls: true
        - 
            name: ge-0/0/13
            unit: 0
            ipv4: '10.11.95.1/30'
            mpls: true
        - 
            name: irb
            unit: 100
            ipv4: '10.21.100.11/24'


ospf:
    areas:
        0.0.0.0:
            interfaces:
                - 
                    name: all
                    ospftype: p2p
                - 
                    name: lo0.0
                    ospftype: passive
                - 
                    name: fxp0.0
                    ospftype: disable
    source-packet-routing:
        node-segment:
            ipv4-index: 11
            index-range: 16384

ldp:
    interfaces:
        - 
            name: all
            mode: enable
        - 
            name: fxp0.0
            mode: disable
            
mpls:
    interfaces:
        - 
            name: all
            mode: enable
        - 
            name: fxp0.0
            mode: disable

    traffic-engineering: bgp-igp-both-ribs
    icmp-tunneling: true



pim:
    interfaces:
        - 
            name: all
            mode: sparse
        - 
            name: fxp0.0
            mode: disable


bgp:
    asn: 64000
    groups:
        rr1:
            bgp_type: internal
            multihop: true
            local-address: 10.0.0.11
            families:
                - evpn
            is_rr: true
            cluster_id: 10.0.1.11
            neighbors:
                - 10.0.0.21
                - 10.0.0.22
                - 10.0.0.23
                - 10.0.0.24
                - 10.0.0.25
                - 10.0.0.26
                - 10.0.0.28

        rr2:
            bgp_type: internal
            multihop: true
            local-address: 10.0.1.11
            families:
                - evpn
                - inet-vpn
            is_rr: true
            cluster_id: 10.0.1.11
            neighbors:
                - 10.0.0.93
                - 10.0.0.94
                - 10.0.0.95
                - 10.0.0.27


services:
    evpn5:
        -
            name: evpn1-type-5-mpls
            interfaces: 
                - irb.110
            rd: 10.0.0.11:110
            rt: 64000:110

    evpn2-bd:
        - 
            name: evpn1-vxlan-bd
            interfaces:
                - ge-0/0/6.0
            rd: 10.0.0.11:100
            rt: 64200:100
            vni-list: 
                - 10100
            bridge-domain:
                -
                    vlan: 100
                    l3-interface: irb.100
                    vni: 10100
                    replication: ingress-node-replication

    l3vpn:
        - 
            name: vrf-1
            rd: 10.0.0.11:901
            rt: 64000:901
            interfaces:
                - lo0.901

    

