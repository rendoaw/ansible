Playbook collection that compatible with latest ansible (at this time is 2.5.0)

## Setup

* setup using virtualenv

```
git clone git@gitlab.com:rendoaw/ansible.git
cd ansible
virtual venv
. venv/bin/activate
pip install -r requirements.txt
```


## Sample run

* Generate JunOS configlet (work in progress)

```
# ansible-playbook -vv -i hosts.lab --extra-vars var_hosts=vmx-11-spine playbooks/pb_configlet_junos.yaml
```


