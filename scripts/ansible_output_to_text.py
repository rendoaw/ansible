#!/usr/bin/env python

#to convert ansible json output back to a normal text files


import sys
import re
import argparse
import json
import os

def arguments_parser():
    parser = argparse.ArgumentParser(description="Options:")
    parser.add_argument('--dir-in', help='input directory' )
    parser.add_argument('--dir-out', help='output directory' )
    parser.set_defaults(detail=False)
    args = parser.parse_args()
    return args


def list_file(dirname):
    files = os.listdir(dirname)
    return files

def read_file(filename):
    cmds = ""
    if filename is not '':
        finput = open(filename)
        lines = [x.replace('\n', '') for x in finput]
        finput.close()
    return lines


def write_file(filename, data):
    if filename is not '':
        f = open(filename, 'w')
        f.write(data)
        f.close()
    return 


def make_human_format(f, fo):
    lines = read_file(f)
    text = ""
    for line in lines:
        line = line.encode('utf-8')
        if line.strip() == "[":
            continue
        elif line.strip() == "]":
            continue
        line = line.strip()
        if line.startswith('"'):
            line = line[1:]
        if line.endswith('"'):
            line = line[:-1]
        xlines = line.split("\\n")
        for xline in xlines:
            text = text + xline + "\n"
    
    write_file(fo, text)


def print_usage():
    print sys.argv[0] + " --dir-in <input dir> --dir-out <output dir>"



if __name__ == "__main__":
    
    args = arguments_parser()
    if not args.dir_in or not args.dir_out:
        print_usage()
        sys.exit(9)

    files_in = list_file(args.dir_in)
    for f in files_in:
        make_human_format(args.dir_in+'/'+f, args.dir_out+'/'+f)




